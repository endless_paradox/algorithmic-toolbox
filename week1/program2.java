import java.util.*;

class program2  {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long input = sc.nextLong();
        ArrayList<Long> arr = new ArrayList<Long>();
        for(int i=0;i<input;i++)   {
            long in = sc.nextLong();
            arr.add(in);
        }
        long res = MaximumParity(arr);
        System.out.println(res);
        sc.close();
    }

    public static long MaximumParity(ArrayList<Long> arr)    {
        long maximum_num1 = 0;
        long maximum_num1_index = 0;
        for(int i=0;i<arr.size();i++)   {
            if(arr.get(i)>maximum_num1) {
                maximum_num1 = arr.get(i);
                maximum_num1_index = i;
            }
        }
        long maximum_num2 = 0;
        for(int i=0;i<arr.size();i++)   {
            if(i!= maximum_num1_index && arr.get(i)>maximum_num2)   {
                maximum_num2=arr.get(i);
            }
        }
        return maximum_num1*maximum_num2;
    }
}