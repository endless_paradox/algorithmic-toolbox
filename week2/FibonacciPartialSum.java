import java.util.*;

public class FibonacciPartialSum {
    public static long sum(long n, long m)  {
        long num1 = n;
        long num2 = m;
        long sum = 0;
        ArrayList<Long> arr = new ArrayList<Long>();
        arr.add((long)0);
        arr.add((long)1);
        for(int i=2;i<=num2;i++) {
            arr.add(arr.get(i-1)+arr.get(i-2));
        }
        for(int i = (int)num1;i<=num2;i++)    {
            sum = sum+arr.get(i);
        }
        return sum%10;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num1 = sc.nextLong();
        long num2 = sc.nextLong();
        if(num1==0 && num2==0)  {
            System.out.println(0);
            System.exit(0);
        }
        else if(num1>num2)   {
            System.out.println(0);
            System.exit(0);
        }
        num2 = num2%60;
        num1 = num1%60;
        if(num2<num1)   {
            num2 = num2+60;
        }
        long res = sum(num1, num2);
        System.out.println(res);
        sc.close();
    }
}

