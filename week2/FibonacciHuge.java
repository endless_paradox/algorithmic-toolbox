import java.util.*;

class FibonacciHuge {
    public static long huge(long n1) {
        long c = 0;
        long previous = 0;
        long current = 1;
        for(long i = 0 ;i<(n1*n1);i++) {
            c = (current+previous)%n1;
            previous = current;
            current = c;
            if(previous==0 && current==1)  {
                return i+1;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num1 = sc.nextLong();
        long num2 = sc.nextLong();
        long res = huge(num2);
        //System.out.println(res);
        num1 = num1%res;
        //System.out.println(num1);
        long n1 = 0;
        long n2 = 1;
        long result = num1;
        for(long i=2;i<=num1;i++)    {
            result = (n1+n2)%num2;
            n1 = n2;
            n2 = result;
        }
        System.out.println(result%num2);
        sc.close();
    }
}

