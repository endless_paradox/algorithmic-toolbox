import java.util.*;

public class FibonacciSumSquares {
    public static void main(final String[] args) {
        final Scanner sc = new Scanner(System.in);
        long n = sc.nextLong();
        n = n % 60;
        if (n == 0) {
            System.out.println(0);
            System.exit(0);
        } else if (n == 1) {
            System.out.println(1);
            System.exit(0);
        }
        final long res = fiboSqure(n);
        System.out.println(res);
        sc.close();
    }

    private static long fiboSqure(final long n) {
        ArrayList<Long> arr = new ArrayList<Long>();
        arr.add((long)0);
        arr.add((long)1);
        long sum = 0;
        for(int i=2;i<=n;i++)   {
            arr.add((arr.get(i - 1) + arr.get(i - 2))%10);
        }
        for(int i=0;i<=n;i++)   {
            sum += Math.pow((long)arr.get(i),2);
        }
        return sum%10;
    }
}

