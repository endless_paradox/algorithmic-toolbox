import java.util.*;

class FibonacciSumLastDigit {
    public static long last(long n)  {
        long res = LastDigitFibo(n);
        res = res%10;
        return res;
    }

    public static long LastDigitFibo(long n)    {
        long current = 1;
        long previous = 0;
        n = n%60;
        long sum = current+previous;
        if(n==0)    {
            return previous;
        }
        if(n==1)    
            return current;
        long res = current+previous;
        for(int i=2;i<=n;i++)   {
            res = (current + previous)%10;
            previous = current;
            current = res;
            sum = sum+res;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        long result = last(num);
        System.out.println(result);
        sc.close();
    }
}

