import java.util.*;

class Fibonacci  {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long res = fib(n);
        System.out.println(res);
        sc.close();
    }

    public static long fib(int n)   {
        ArrayList<Long> arr = new ArrayList<Long>();
        arr.add((long)0);
        arr.add((long)1);
        for(int i=2;i<=n;i++)    {
            arr.add(arr.get(i-1)+arr.get(i-2));
        }
        return arr.get(n);
    }
}