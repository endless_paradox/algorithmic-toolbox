import java.util.*;


class FibonacciLastDigit    {
    public static int last(int n)   {
        long num1 = 0;
        if(n==0)    {
            return (int)num1;
        }
        long num2 = 1;
        if(n==1)    {
            return (int)num2;
        }
        long res = 0;
        for(int i=2 ;i<=n;i++)   {
            res = (num1+num2)%10;
            num1 = (num2)%10;
            num2 = res;
        }
        return (int)res%10;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int res = last(n);
        System.out.println(res);
        sc.close();
    }
}

