import java.util.*;

class LCM {
  public static long least(long n1, long n2)  {
    long num1 = n1;
    long num2 = n2;
    long res = 0;
    while(num2!=0)  {
      long num3 = num1;
      num1 = num2;
      num2 = num3%num2;
      if(num2==0) {
        res = num1;
      }
    }
    n1 = n1/res;
    n2 = n2/res;
    return n1*n2*res;
  }
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long num1 = sc.nextLong();
    long num2 = sc.nextLong();
    long res = least(num1, num2);
    System.out.println(res);
    sc.close();
  }
}
